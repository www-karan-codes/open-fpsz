# open-fpsz

We love fpsz video games, but we're tired of the genre being abandoned by prominent studios. To properly honor its legacy, we've chosen to develop our own game inspired by it, aiming to surpass its qualities with familiar jetpack and skiing mechanics.

## Prerequisities

Download `Godot Engine` at https://godotengine.org/download

## Getting started

1. Clone the repository
2. Open Godot Engine
3. Navigate to the repository and import the `project.godot`
4. Start coding or run the project!

## Contributing

Before contributing, please take a moment to review our [Contribution Guidelines](CONTRIBUTING.md) in this repository. These guidelines outline best practices, coding standards, and other important information to ensure that your contributions align with the project's goals and maintain consistency across the codebase.

We welcome contributions from the community to help improve and expand the project. Feel free to submit pull requests, report bugs, or share ideas for new features.

## License

This project is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/). For more details, please refer to the [LICENSE](LICENSE) file.
