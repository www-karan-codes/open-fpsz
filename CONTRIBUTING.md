[[_TOC_]]

# How to Contribute?

Thank you for considering contributing to our project! 

Whether you're an experienced developer or just starting out, we welcome your contributions to help make our game even better.

## Reporting Issues

If you encounter any bugs or issues, please report them in the [Issue Tracker](https://gitlab.com/godotribes/godotribes/-/issues). Include as much detail as possible, such as steps to reproduce the issue and expected vs. actual behavior.

## Merge Requests and Releases

We highly recommend using [gitmoji](https://gitmoji.dev) for expressive and visually appealing commit messages, as it provides an easy way of identifying the purpose or intention of a commit simply by looking at the emojis used.

Every **merge request** (MR) must be merged into the `develop` branch before any release is made on the `main` branch. 

The `develop` branch serves as the staging area for upcoming features and fixes.

When the `develop` branch is deemed stable and ready for release, it is merged into the `main` branch to create a new release.

This practice ensures that everyone remains updated on ongoing tasks, fostering transparency and encouraging collaboration within the team.

# Development Guidelines

## Code Style Guide

For consistency across the source code, we *must* follow the [Godot Engine Style Guide](https://docs.godotengine.org/en/stable/tutorials/best_practices/project_organization.html#style-guide) at any time:

>- Use **snake_case** for folder and file names (with the exception of C#
>  scripts). This sidesteps case sensitivity issues that can crop up after
>  exporting a project on Windows. C# scripts are an exception to this rule,
>  as the convention is to name them after the class name which should be
>  in PascalCase.
>- Use **PascalCase** for node names, as this matches built-in node casing.
>- In general, keep third-party resources in a top-level `addons/` folder, even
>  if they aren't editor plugins. This makes it easier to track which files are
>  third-party. There are some exceptions to this rule; for instance, if you use
>  third-party game assets for a character, it makes more sense to include them
>  within the same folder as the character scenes and scripts.

## Branch Naming Convention

When working on a new *feature*, prefix the branch name with `feat/`. For *bug fixes*, use the prefix `fix/`.

This naming convention helps to categorize branches and makes it easier to identify their purpose at a glance.

# Git Quick Reference

1. Create a new branch for your changes:

```shell
git checkout -b fix/my-branch
```

2. Make your changes, stage then and commit:

```shell
git commit -am "📝 update CONTRIBUTIONS.md"
```

3. Push your changes to the repository:

```shell
git push
```

4. Create a [Merge Request (MR)](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) with a clear description of your changes

## Excluding local files without creating a _.gitignore_ file

If you don't want to add new rules in a `.gitignore` file to be shared with everyone, you can create *exclusion rules* that are not committed with the repository. You can use this technique for locally-generated files that you don't expect other users to generate, such as files created by your editor.

Use your favorite text editor to open the file called `.git/info/exclude` within the root of your git repository. Any rule you add here will not be checked in, and will only ignore files for your local repository.

---

By following these guidelines, we aim to streamline our development process, maintain code quality, and ensure that our releases are stable and reliable.

Happy coding! 🎮✨
